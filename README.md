# AnalysisPayload

This is the primary analysis payload starting point for the ATLAS-based sections 
f the tutorial.  It has been included here with a CMakeLists.txt file that assumes 
you will be using it in tandem with the [JetSelectionHelper](https://gitlab.cern.ch/usatlas-computing-bootcamp/JetSelectionHelper)
package.  An example of this usage is provided in [v3-gitmodule-submodule-jetselector](https://gitlab.cern.ch/usatlas-computing-bootcamp/v3-gitmodule-submodule-jetselector).

If you want to compile this as a standalone and basic package, refer to the [v0-prework-code](https://gitlab.cern.ch/usatlas-computing-bootcamp/v0-prework-code)
package which provides directions for CMake-based or standalone compilation.